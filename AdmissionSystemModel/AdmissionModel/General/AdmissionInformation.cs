﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;





namespace AdmissionSystemModel.AdmissionModel
{
 public partial class AdmissionInformation
    {

        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }

        public string Session { get; set; }

        public IEnumerable<string> Type { get; set; }
        public IEnumerable<string> RollNumber { get; set; }

        public IEnumerable<string> Year { get; set; }
        public IEnumerable<string> Institute { get; set; }

        public IEnumerable<string> subjectID { get; set; }
        public IEnumerable<string> status { get; set; }
        public IEnumerable<string> Degree { get; set; }


        public IEnumerable<PeperSelectDetailAdmission> detailistSubject { get; set; }
        public IEnumerable<ExammDetailAdmission> detailistExam { get; set; }


        public HttpPostedFileBase ImageUpload { get; set; }


        public List<AdmissionInformation> getStudentData()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {

                    return context.AdmissionInformations.ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public int addata(AdmissionInformation obj)
        {
            try
            {

                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    context.AdmissionInformations.Add(obj);
                    context.SaveChanges();
                    //  return obj.AdmisssionID;
                    int a = 0;
                    foreach (var item in obj.subjectID)
                    {
                        PeperSelectDetailAdmission objdss = new PeperSelectDetailAdmission();
                        objdss.subectID = Convert.ToInt32( item);

                        objdss.status = obj.status.ToArray()[a] == "0" ? false : true;

                        objdss.admissionID = obj.AdmisssionID;
                        a++;
                        context.PeperSelectDetailAdmissions.Add(objdss);
                        context.SaveChanges();
                        
                    }
                    int b = 0;
                    foreach (var item in obj.Type)
                    {
                        ExammDetailAdmission objd = new ExammDetailAdmission();
                        objd.Type = item;
                        objd.Degree = obj.Degree.ToArray()[b];

                        objd.RollNumber = obj.RollNumber.ToArray()[b];
                        objd.Year =Convert.ToInt32( obj.Year.ToArray()[b]);
                        objd.RollNumber = obj.RollNumber.ToArray()[b];
                        objd.Institute = obj.Institute.ToArray()[b];
                        objd.AdmissionID = obj.AdmisssionID;
                        b++;
                        context.ExammDetailAdmissions.Add(objd);
                        context.SaveChanges();

                    }
                }
              




                    return obj.AdmisssionID;

            }
            catch (Exception ex)
            {
             
                return 0;
            }
        }



        public int UpdateData(AdmissionInformation obj)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var result =context.AdmissionInformations.SingleOrDefault(x=>x.AdmisssionID==obj.AdmisssionID);
                    if (result!=null)
                    {
                        result.SessionID = obj.SessionID;
                        result.Rollno = obj.Rollno;
                        result.RegistrationNo = obj.RegistrationNo;
                        result.InstituteName = obj.InstituteName;
                        result.AdmissionDate = obj.AdmissionDate;
                        result.Name = obj.Name;
                        result.FatherName = obj.FatherName;
                        result.DOB = obj.DOB;
                        result.Cnic = obj.Cnic;
                        result.PhoneNo = obj.PhoneNo;
                        result.PhotoPath = obj.PhotoPath;
                        result.status = obj.status;
                        result.gender = obj.gender;
                        result.Religion = obj.Religion;
                        result.Partid = obj.Partid;
                        result.Address = obj.Address;
                        result.PerAddress = obj.PerAddress;

                        context.SaveChanges();

                        List<ExammDetailAdmission> resultofDOC = getExaamdetail(obj.AdmisssionID);
                        if (resultofDOC != null)
                        {
                            context.ExammDetailAdmissions.RemoveRange(context.ExammDetailAdmissions.Where(c => c.AdmissionID == obj.AdmisssionID));

                        }
                        context.SaveChanges();
                        List<PeperSelectDetailAdmission> resultosubjectt= getSubjjectdetaiil(obj.AdmisssionID);
                        if (resultofDOC != null)
                        {
                            context.PeperSelectDetailAdmissions.RemoveRange(context.PeperSelectDetailAdmissions.Where(c => c.admissionID == obj.AdmisssionID));

                        }

                        context.SaveChanges();

                        int a = 0;
                        foreach (var item in obj.subjectID)
                        {
                            if (item != "0")
                            {
                                
                                PeperSelectDetailAdmission objdss = new PeperSelectDetailAdmission();
                                objdss.subectID = Convert.ToInt32(item);

                                objdss.status = obj.status.ToArray()[a] == "0" ? false : true;
                                objdss.admissionID = obj.AdmisssionID;
                                a++;
                                context.PeperSelectDetailAdmissions.Add(objdss);
                                context.SaveChanges();
                            }

                        }
                        int b = 0;
                        foreach (var item in obj.Type)
                        {
                            ExammDetailAdmission objd = new ExammDetailAdmission();
                            objd.Type = item;
                            objd.Degree = obj.Degree.ToArray()[b];

                            objd.RollNumber = obj.RollNumber.ToArray()[b];
                            objd.Year = Convert.ToInt32(obj.Year.ToArray()[b]);
                            objd.RollNumber = obj.RollNumber.ToArray()[b];
                            objd.Institute = obj.Institute.ToArray()[b];
                            objd.AdmissionID = obj.AdmisssionID;
                            b++;
                            context.ExammDetailAdmissions.Add(objd);
                            context.SaveChanges();

                        }




                    }
                    return result.AdmisssionID;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }


        public bool DeleteData(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var result = context.AdmissionInformations.SingleOrDefault(x => x.AdmisssionID == id);
                    if (result != null)
                    {
                        context.AdmissionInformations.Remove(result);
                        context.SaveChanges();


                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public AdmissionInformation getAllSession(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    return context.AdmissionInformations.SingleOrDefault(x => x.AdmisssionID == id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List< ExammDetailAdmission>getExaamdetail(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    return context.ExammDetailAdmissions.Where(x => x.AdmissionID == id).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
       
        public List<PeperSelectDetailAdmission> getSubjjectdetaiil(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    return context.PeperSelectDetailAdmissions.Where(x => x.admissionID == id).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public AdmissionInformation getAllMasterData(int id)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {

                    return context.AdmissionInformations.SingleOrDefault(x=>x.AdmisssionID==id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public List<sp_AllAdmissionData_Result> getAllData(int id =0)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {

                   return context.sp_AllAdmissionData(id).ToList();

  


                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        //public List<ListClass> GetOutletsbytotid(long totid)
        //{
        //    try
        //    {
        //        using (var context = new SalesOperationsEntities())
        //        {
        //            return (from r in context.Outlets
        //                    join d in context.TOT_Injection on r.OutletId equals d.Outlet_Id
        //                    join t in context.TOT_Transfer on d.TOT_Id equals t.TOT_Id
        //                    where r.IsActive == true && t.TOT_Id == totid && t.End_Date == null
        //                    select new ListClass { Id = r.OutletId, Title = r.OutletTitle, Details = r.OutletAddress }
        //                  ).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogError(ex);
        //        return null;
        //    }
        //}

        public List<SelectListItem> GetPackageByPart(int id = 0)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {


                    return (from r in context.StudentPackageInfoes
                            join d in context.StudentPrograms on r.PartID equals d.ProgramID
                           
                            where  r.PartID==id
                            select new SelectListItem { Value = r.PackageID.ToString(), Text = d.title +" - "+  r.PackageTitle  }
                         ).ToList();
                   /// return context.StudentPackageInfoes.Where(x => x.PartID == id).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<SelectListItem> GetStudentNameList()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {


                    return (from r in context.AdmissionInformations
                            select new SelectListItem { Value = r.AdmisssionID.ToString(), Text = r.Rollno + " - " + r.Name }
                         ).ToList();
                    /// return context.StudentPackageInfoes.Where(x => x.PartID == id).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<ExammDetailAdmission> getExamdetailData(int id = 0)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {

                    return context.ExammDetailAdmissions.Where(x=>x.AdmissionID==id).ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public IEnumerable<SelectListItem> getstatusValue()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var list = context.StudentTypes.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Session--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.title, Value = item.StudentTypeID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public IEnumerable<SelectListItem> getDistrictList()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var list = context.DistrictInfoes.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select District--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.DirtrictTitle, Value = item.DistrictID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public IEnumerable<SelectListItem> getPartValue()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var list = context.StudentPrograms.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Part--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.title, Value = item.ProgramID.ToString() });
                    }


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public IEnumerable<SelectListItem> getAllSessionValue()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var list= context.StudentSessions.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Session--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.title , Value = item.StudentSessionID.ToString() });
                    }
                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }



        public IEnumerable<SelectListItem> getAllSubjject()
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    var list = context.StudetSubjects.ToList();
                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "--Select Session--", Value = "0" });
                    foreach (var item in list)
                    {
                        listobj.Add(new SelectListItem { Text = item.title, Value = item.StudetSubjectID.ToString() });
                    }
                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List< AdmissionInformation> checkDuplicate(int id ,string title)
        {
            try
            {
                using (var context = new OnlineAdmisssionSystemEntities())
                {
                    if (id>0)
                    {
                        return context.AdmissionInformations.Where(x => x.Cnic == title && x.AdmisssionID != id).ToList();

                    }
                    else
                    {
                        return context.AdmissionInformations.Where(x => x.Cnic == title ).ToList();


                    }

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

    }


}


//@using System.Globalization
//@using SalesOperations.Common
//@model List<SalesOperations.Models.SP_GetOrderBookingPosting_Result>
//@{
//    int rowno = 0;
//    List<SalesOperations.Models.SP_GetOrderBookingPosting_Main_Result> obj_main = new SalesOperations.Models.OrderBooking().GetOrderBookingPostingViaRoute(ViewData["routeId"].ToString(), Convert.ToDateTime(ViewData["ObDate"]));

//    < table class="table" id="main-table">
//        <thead>
//            <tr>
//                <td> Post</td>
//                <td> Outlets</td>
//                <td> OrderBooking DateTime</td>
//                <td> Remarks</td>
//                <td>  </td>
//                <td>Status</td>
//                <td>Action</td>
//            </tr>

//        </thead>
//        <tbody>


//            @foreach (var obj in obj_main)
//{
//                < tr id = "row-@rowno" >
 
//                     < td >
 
//                         < input type = "checkbox" class="input" checked="checked" id="main-@obj.OrderBookingPostingId" value="@obj.OrderBookingPostingId">
//                    </td>

//                    <td>

//                        @obj.OutletTitle

//                    </td>
//                    <td>
//                        @obj.OrderBookingDate.ToString(Constants.DATE_FORMAT_SERVER_SIDE) - @obj.OrderBookingTime

//                    </td>
//                    <td colspan = "2" > @obj.Remarks </ td >


//                    < td >

//                        @obj.StatusTitle
//                    </ td >
//                    < td >
//                        < button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#@obj.OrderBookingPostingId" aria-expanded="false" aria-controls="collapseExample">
//                            View Details
//                        </button>
//                    </td>



//                </tr>
//                rowno = rowno + 1;
//                <tr id = "row-@rowno" >
//                    < td colspan="2">
//                        <div class="collapse" id="@obj.OrderBookingPostingId">
//                            <div class="well">



//                                <table class="table" id="@obj.OrderBookingPostingId-detail">
//                                    <thead>
//                                        <tr>
//                                            <th rowspan = "2" > Post </ th >
//                                            < th rowspan="2" class="text-align-top">Brand Pack</th>
//                                             <th colspan = "2" class="text-center">Quantity</th>
//                                        </tr>
//                                        <tr>

//                                            <th class="text-center">C/S.</th>
//                                            @*<th>

//                                                BT.

//                                            </th>*@

//                                        </tr>

//                                    </thead>


//                                    <tbody>
//                                        @foreach(var objects in Model)
//{
//    if (obj.OrderBookingPostingId == objects.OrderBookingPostingId)
//    {
//                                                < tr >
//                                                    < td >
//                                                        < input type = "checkbox" class="input" checked="checked" id="detail-@objects.OrderBookingPostingDetailId" value="@objects.OrderBookingPostingDetailId">
//                                                    </td>
//                                                    <td>
//                                                        @objects.BrandPackTitle
//                                                    </td>
//                                                    <td class="text-center">
//                                                        <input type = "text" class="input text-center" value="@objects.Q_Cases" readonly="readonly" style="width: 60px;">

//                                                    </td>
//                                                    @*<td>
//                                                        <input type = "number" class="input" value="@objects.Q_Btl" readonly="readonly" style="width: 50px;">

//                                                    </td>*@
//                                                </tr>

//                                            }

//                                        }

//                                    </tbody>
//                                </table>
//                            </div>
//                        </div>

//                </tr>

//                rowno = rowno + 1;
//            }

//        </tbody>








//    </table>

//}