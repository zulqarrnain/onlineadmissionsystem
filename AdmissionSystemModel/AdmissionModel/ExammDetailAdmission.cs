//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdmissionSystemModel.AdmissionModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExammDetailAdmission
    {
        public int ExamID { get; set; }
        public Nullable<int> AdmissionID { get; set; }
        public string Type { get; set; }
        public string RollNumber { get; set; }
        public Nullable<int> Year { get; set; }
        public string Institute { get; set; }
        public string Degree { get; set; }
    }
}
