﻿using AdmissionSystemModel.AdmissionModel;
using Newtonsoft.Json;
using SAGERPNEW2018.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAGERPNEW2018.Controllers
{
    public class AdmissionInformationController : Controller
    {
        // GET: AdmissionInformation
        [Authorize]
        public ActionResult Index()
        {
            AdmissionInformation a = new AdmissionInformation();

            DataTable dt = loadrighta();
            if (dt.Rows.Count > 0)
            {
                a.Isdelete = Convert.ToBoolean(dt.Rows[0]["Isdelete"]);
                a.IsNew = Convert.ToBoolean(dt.Rows[0]["IsNew"]);
                a.IsPrint = Convert.ToBoolean(dt.Rows[0]["IsPrint"]);
                a.Isedit = Convert.ToBoolean(dt.Rows[0]["Isedit"]);
                a.IsAsign = Convert.ToBoolean(dt.Rows[0]["Assign"]);
                if (!a.IsAsign)
                {
                    TempData["NORights"] = "You Have No Rights To Perform This Action";
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(a);
        }
        [Authorize]
        public ActionResult create(AdmissionInformation a)
        {
            DataTable dt = loadrighta();
            if (dt.Rows.Count > 0)
            {
                a.Isdelete = Convert.ToBoolean(dt.Rows[0]["Isdelete"]);
                a.IsNew = Convert.ToBoolean(dt.Rows[0]["IsNew"]);
                a.IsPrint = Convert.ToBoolean(dt.Rows[0]["IsPrint"]);
                a.Isedit = Convert.ToBoolean(dt.Rows[0]["Isedit"]);
                a.IsAsign = Convert.ToBoolean(dt.Rows[0]["Assign"]);
                if (!a.IsAsign)
                {
                    TempData["NORights"] = "You Have No Rights To Perform This Action";
                    return RedirectToAction("Index", "Home");
                }
            }
            ViewData["Editmode"]=false;
            a.detailistSubject = a.getSubjjectdetaiil(-1);
            a.detailistExam = a.getExamdetailData(-1);
            a.PhotoPath = "~/AppFiles/Images/placeholder-avatar.jpg";
            
            return View(a);
        }
        [Authorize]
        public ActionResult delete(int id)
        {
            AdmissionInformation a = new AdmissionInformation();
            bool c = a.DeleteData(id);
            if (c)
            {
                return RedirectToAction("Index");

            }
            TempData["Dependancy"] = "This Record Using In Another Table";
            return RedirectToAction("Index");
        }
        [Authorize]
        public ActionResult Edit(string id)
        {
            string[] ID = id.Split('|');

            AdmissionInformation a = new AdmissionInformation();
            a = a.getAllMasterData(Convert.ToInt32(ID[0]));
            a. detailistSubject = a.getSubjjectdetaiil(Convert.ToInt32(ID[0]));
            a. detailistExam = a.getExamdetailData(Convert.ToInt32(ID[0]));

            ViewData["Editmode"] = true;

            if (ID[1] == "0")
            {
                a.IsView = true;
            }
            return View("create", a);
        }

        [Authorize]
        public ActionResult Save(AdmissionInformation model)
        {

            if (model.ImageUpload != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(model.ImageUpload.FileName);
                string extension = Path.GetExtension(model.ImageUpload.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                model.PhotoPath = "~/AppFiles/Images/" + fileName;
                model.ImageUpload.SaveAs(Path.Combine(Server.MapPath("~/AppFiles/Images/"), fileName));
            }



            int check;
            if (model.AdmisssionID > 0)
            {
                check = check = model.UpdateData(model);
            }
            else
            {

                check = model.addata(model);
            }

            if (check > 0)
            {
                return RedirectToAction("Index");

            }
            return RedirectToAction("create", model);
        }

        public JsonResult Duplicate(string Name, int ID)
        {
            string json = "";
            var list = new AdmissionInformation().checkDuplicate(ID, Name);
            if (list.Count() > 0)
            {
                json = " Duplicate Record Found ";
            }
            return Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet);
        }


        public JsonResult LoadPackages(int ID)
        {
           
            var list = new AdmissionInformation().GetPackageByPart( ID);
         
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        private DataTable loadrighta()
        {
            string[] userdata = new Login().userinfofromCookie();
            DataTable dtright = new Login().checkRightUser(" where GLUser.Userid='" + userdata[1] + "' and UserForms.Formid='29' ");
            return dtright;
        }
    }
}