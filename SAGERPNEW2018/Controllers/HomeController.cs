﻿using AdmissionSystemModel.AdmissionModel;
using SAGERPNEW2018.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SAGERPNEW2018.Controllers
{
    public class HomeController : Controller
    {
       // string constring = ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
        [Authorize]
        public ActionResult Index()
        {

            var student = new AdmissionSystemModel.AdmissionModel.AdmissionInformation().getStudentData();
            ViewBag.part1 = student.Where(x => x.Partid == 1).Count();
            ViewBag.part2 = student.Where(x => x.Partid == 2).Count();
            ViewBag.part3 = student.Where(x => x.Partid == 3).Count();

            ViewBag.Mgender1 = student.Where(x => x.gender == false && x.Partid == 1).Count();
            ViewBag.Fgender1 = student.Where(x => x.gender == true && x.Partid == 1).Count();

            ViewBag.Mgender2 = student.Where(x => x.gender == false && x.Partid == 2).Count();
            ViewBag.Fgender2 = student.Where(x => x.gender == true && x.Partid == 2).Count();

            ViewBag.Mgender3 = student.Where(x => x.gender == false && x.Partid == 3).Count();
            ViewBag.Fgender3 = student.Where(x => x.gender == true && x.Partid == 3).Count();

            var Package = new AdmissionSystemModel.AdmissionModel.StudentPackageInfo().getAllPackages();
            ViewBag.Packagepart1 = Package.Where(x => x.PartID == 1);
            ViewBag.Packagepart2 = Package.Where(x => x.PartID == 2);
            ViewBag.Packagepart3 = Package.Where(x => x.PartID == 3);
            return View();
        }

        public ActionResult FlotCharts()
        {
            return View("FlotCharts");
        }

        public ActionResult MorrisCharts()
        {
            return View("MorrisCharts");
        }

        public ActionResult Tables()
        {
            return View("Tables");
        }

        public ActionResult Forms()
        {
            return View("Forms");
        }

        public ActionResult Panels()
        {
            return View("Panels");
        }

        public ActionResult Buttons()
        {
            return View("Buttons");
        }

        public ActionResult Notifications()
        {
            return View("Notifications");
        }

        public ActionResult Typography()
        {
            return View("Typography");
        }

        public ActionResult Icons()
        {
            return View("Icons");
        }

        public ActionResult Grid()
        {
            return View("Grid");
        }

        public ActionResult Blank()
        {
            return View("Blank");
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            Loginform log = new Loginform();
            FormsAuthentication.SignOut();
            return View(log);
        }
        String ipsystem;

        public ActionResult CheckLogin(Loginform a)

        {
            ///SqlConnection con = new SqlConnection(constring);
            string system= Environment.MachineName ;
            system+="|"+ Environment.UserName;
            IPAddress[] ipAddress = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress item in ipAddress)
            {
                if (item.AddressFamily==AddressFamily.InterNetwork)
                {
                    ipsystem = "|" + item.ToString();
                }
            }
            system+=ipsystem;
            var logindata=new GLUser().UserLogin(a.Username, a.Password);
            if (logindata!=null)
            {
                FormsAuthentication.SetAuthCookie(a.Username + "|" + logindata.Userid.ToString() + "|" + logindata.Type.ToString() +
                    "|" + system, false);
                return RedirectToAction("Index");
            }
            else
            {
                TempData["LoginFailed"] = "Try Again Incorrect username or password";
                return View("Login", a);

            }


            //DataTable dt = new DataTable();
            //SqlDataAdapter da = new SqlDataAdapter(@" select * from gluser where GLUser.UserName='" + a.Username + "' and  GLUser.UserPassword='" + a.Password + "'", con);
            //da.Fill(dt);
            //if (dt.Rows.Count > 0)
            //{
            //    FormsAuthentication.SetAuthCookie(a.Username +"|"+ dt.Rows[0]["userid"].ToString() +"|"+ dt.Rows[0]["type"].ToString() + 
            //        "|"+ a.FiscalID+"|"+ a.Companyid+"|"+ system , false);
              
            //    Session["rightdt"] = dt;
            //    Session["UserRight"] = dt.Rows[0]["type"].ToString();
            //    Session["userid"] = dt.Rows[0]["userid"].ToString();
            //    Session["FiscalID"] = a.FiscalID;
            //    Session["Companyid"] = a.Companyid;
            //    Session["system"] = system;
            //    return View("Index",a);
            //}
            //else
            //{
            //    TempData["LoginFailed"] = "Try Again Incorrect username or password";
            //    return View("Login",a);

            //}
        }
    }
}