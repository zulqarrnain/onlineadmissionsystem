﻿using SAGERPNEW2018.Domain;
using SAGERPNEW2018.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAGERPNEW2018.Controllers
{
    public class NavbarController : Controller
    {
        // GET: Navbar
        public ActionResult Index()
        {
            Navbar nav = new Navbar();
            var data = new Data();
          string[] user  = new Login().userinfofromCookie();
        
            Session["UserName"] = user[0];
        
            return PartialView("_Navigation", data.navbarItems().ToList());
        }
    }
}