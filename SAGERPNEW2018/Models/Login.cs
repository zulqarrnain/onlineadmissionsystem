﻿using AdmissionSystemModel.AdmissionModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SAGERPNEW2018.Models
{
    public class Login
    {

        string constring = ConfigurationManager.ConnectionStrings["OnlineAdmisssionSystemEntities"].ConnectionString;

        public string Username { get; set; }
        public string Password { get; set; }
        public int Companyid { get; set; }
        public int FiscalID { get; set; }

        public bool type { get; set; }

     
        public string[] userinfofromCookie()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName]; 
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            string cookiePath = ticket.CookiePath;
            DateTime expiration = ticket.Expiration;
            bool expired = ticket.Expired;
            bool isPersistent = ticket.IsPersistent;
            DateTime issueDate = ticket.IssueDate;
            string name = ticket.Name;
            string userData = ticket.UserData;
            int version = ticket.Version;
            string[] a= name.Split('|');
            return a;
        }

        public DataTable checkRightUser(string where="")
        {
            using (var context = new OnlineAdmisssionSystemEntities())
            {
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(@"SELECT     GLUser.type,  GLUser.Userid, GLUser.UserName, GLUser.UserPassword, GLUser.GroupID, GLUser.Active, GLUserGroup.GroupTitle, GLUserGroup.Description, GLUserGroup.Inactive, GLUserGroupDetail.Assign, GLUserGroupDetail.IsEdit, 
                         GLUserGroupDetail.IsDelete, GLUserGroupDetail.IsPrint, GLUserGroupDetail.Isnew, UserForms.FormTitle, UserForms.Formid
                            FROM            GLUser INNER JOIN
                         GLUserGroup ON GLUserGroup.GroupID = GLUser.GroupID INNER JOIN
                         GLUserGroupDetail ON GLUserGroupDetail.UserGroupID = GLUserGroup.GroupID INNER JOIN
                         UserForms ON UserForms.Formid = GLUserGroupDetail.FormsID " + where + "", context.Database.Connection.ConnectionString.ToString()
                             );
                da.Fill(dt);
                return dt;
            }
        } 


      

    }
}