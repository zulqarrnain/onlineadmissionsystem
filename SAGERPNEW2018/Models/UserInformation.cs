﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAGERPNEW2018.Models
{
    public class UserInformation
    {
        string constring = ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;

        public int Userid { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public int GroupID { get; set; }
        public bool UserType { get; set; }

        public string Entryby { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool Active { get; set; }
        public bool IsView { get; set; }

        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }
        public bool IsAsign { get; set; }




        public DataTable selectall(string where = "", string id = "")
        {
            SqlConnection con = new SqlConnection(constring);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(@" select * ,case active when '1' then 'InActive' else 'Active'  end as status from GLUser 
                                                        inner join GLUserGroup on GLUserGroup.GroupID= GLUser.GroupID " + where, con);
            da.Fill(dt);
            return dt;
        }

     

        public bool save(UserInformation model)
        {
            try
            {

                string[] userdata = new Login().userinfofromCookie();
                SqlConnection db = new SqlConnection(constring);
                SqlCommand com = new SqlCommand();
                com.Connection = db;
                if (model.Userid > 0)
                {
                    db.Open();
                    com.CommandText = @"update [GLUser] set   UserName='" + model.UserName
                        + "', UserPassword='" + model.UserPassword + "',active='" + model.Active +
                        "',Entryby='" + userdata[1] + "' ,TimeStamp='" + DateTime.Now + "'  ,Type=1, groupid='" + model.GroupID + "' where  Userid='" + model.Userid + "'";
                    com.ExecuteNonQuery();
                    db.Close();


                }
                else
                {
                    db.Open();
                    com.CommandText = @" insert into [GLUser]  ([UserName] ,[UserPassword] ,[GroupID]   ,[Entryby]  ,[TimeStamp]  ,[Active] ) values
                        ('" + model.UserName + "','" + model.UserPassword + "','" + model.GroupID + "','" + userdata[5] + "' ,  '" + DateTime.Now + "','" + model.Active + "') ";
                    com.ExecuteNonQuery();
                    db.Close();

                }

            }
            catch (Exception)
            {

                return false;
            }
            return true;

        }


        public IEnumerable<SelectListItem> loadUsergroup(string where = "")
        {
            SqlConnection con = new SqlConnection(constring);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from GLUserGroup" + where, con);
            da.Fill(dt);
            List<SelectListItem> list = new List<SelectListItem>();
            ///  list.Add(new SelectListItem { Text = "--Please Select Branch--", Value = "0" });
            foreach (DataRow item in dt.Rows)
            {
                list.Add(new SelectListItem { Text = item["GroupTitle"].ToString(), Value = item["GroupID"].ToString() });
            }

            return list;
        }

        public bool Deletedrecord(int id)
        {
            SqlConnection con = new SqlConnection(constring);
            SqlCommand cmd = new SqlCommand();
            con.Open();
            cmd.Connection = con;
            cmd.CommandText = "delete from GLUser where Userid=" + id + "";
            cmd.ExecuteNonQuery();
            con.Close();
            return true;
        }
    }
}